


dotnet tool install --global dotnet-sonarscanner

SONAR_TOKEN="sonar cloud token"
dir="$(pwd)"
dotnet sonarscanner begin /k:"senips-sonar:sonar-dotnet" /n:"sonar-dotnet" /v:"#.#.#" /o:"senips-sonar" /d:sonar.host.url="https://sonarcloud.io" /d:sonar.login="${SONAR_TOKEN}" /d:sonar.language="cs" /d:sonar.exclusions="**/bin/**/*,**/obj/**/*" /d:sonar.cs.opencover.reportsPaths="${dir}/lcov.opencover.xml"
dotnet restore
dotnet build
dotnet test /p:CollectCoverage=true /p:CoverletOutputFormat=\"opencover,lcov\" /p:CoverletOutput=../lcov
dotnet sonarscanner end /d:sonar.login="${SONAR_TOKEN}"
